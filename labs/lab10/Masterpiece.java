//=================================================
// Honor Code: The work I am submitting is
// a result of my own thinking and efforts.

// Your Name Kyle Donnelly
// CMPSC 111 Spring 2016
// Lab 11
// Date: 04 21 2016 [fill in the date]
//
// Purpose: To create a digital drawing using methods in various
// files.
//=================================================
import java.awt.*;
import javax.swing.*;

public class Masterpiece extends JApplet
{
    Graphics page;

    public Masterpiece(Graphics p)
    {
        page = p;
    }

    // constructor
    public void drawRectangle(int a,int b){ //Makes square.
    page.setColor(Color.gray);
    page.fillRect( a, b, 20, 20);
    }

    public void drawCircle(int a,int b,int angle1,int angle2){ // Makes semicircle.
    page.setColor(Color.blue);
    page.fillArc(a, b, 20, 20, angle1, angle2);
    }

    public void Line(int a,int b,int w,int z){
    page.setColor(Color.black);
    page.drawLine(a, b, w, z);
    }

    public void drawTriangle(int a, int b){  // Makes equilateral triangle of base 6.
        page.drawLine(a, b, a+6, b);
        page.drawLine(a, b, a+3, b-6);
        page.drawLine(a+3, b-6, a+6, b);
    }
}
