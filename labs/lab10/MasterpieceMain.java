//=================================================
// Honor Code: The work I am submitting is
// a result of my own thinking and efforts.

// Your Name Kyle Donnelly
// CMPSC 111 Spring 2016
// Lab 11
// Date: 04 21 2016 [fill in the date]
//
// Purpose: To create a program that makes a drawing of a sword
// by calling methods in other programs.
//=================================================
import java.awt.*;
import javax.swing.*;
import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.File;
import java.awt.image.BufferedImage;

public class MasterpieceMain extends JApplet
{
    //-------------------------------------------------
    // Use Graphics methods to add content to the drawing canvas
    //-------------------------------------------------
    public void paint(Graphics page)
    {
    final int WIDTH = 400;
    final int HEIGHT = 600;

    page.setColor(Color.white);
    page.fillRect(0,0,WIDTH,HEIGHT);

    Masterpiece d = new Masterpiece(page);

    int x = 190;
    int y = 460;
    d.drawRectangle(x,y);
    d.drawCircle(x-10, y, 90, 180);
    d.drawCircle(x, y+10,0 , -180);
    d.drawCircle(x+10, y, -90, 180);
    d.Line(x, y, x, y-320);
    d.Line(x+10, y, x+10, y-340);
    d.Line(x+20, y, x+20, y-320);
    d.Line(x, y-320, x+10, y-340);
    d.Line(x+20, y-320, x+10, y-340);
    d.drawTriangle(x+4,y-10);
    d.drawTriangle(x+10,y-10);
    d.drawTriangle(x+7, y-16);

    x = 0;
    y = 100;

    do {
    x = 40;
        do {
        d.drawTriangle(x,y-10);
        d.drawTriangle(x+6,y-10);
        d.drawTriangle(x+3, y-16);
        x = x + 100;
        } while (x < 400);

    y = y + 100;
    } while (y < 600);





	// create an instance of Masterpiece class
	// NOTE: you may have to pass 'page' in addition to any
	//       other arguments in your constructor or during your method calls

	// call the methods in the Masterpiece class


    }

    // main method that runs the program
    public static void main(String[] args)
    {
        JFrame window = new JFrame("Kyle Donnelly");

      		// Add the drawing canvas and do necessary things to
     		// make the window appear on the screen!
        	window.getContentPane().add(new MasterpieceMain());
        	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	window.setVisible(true);
            window.setSize(400, 600);




    }
}
