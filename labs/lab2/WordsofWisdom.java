
//****************************************
// Honor Code: This work is mine unless otherwise cited.
// Kyle Donnelly
// COMPSC 111 Spring 2016
// Lab 2
// Date: 01 28 2016
//
// Purpose: To practice using Template files for labs.
//****************************************
import java.util.Date; // needed for printing today's date

public class WordsofWisdom
{
	//--------------------------------
	// main method: program execution begins here
	//--------------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Kyle Donnelly\n Lab 2\n" + new Date() + "\n");
		System.out.println("The following statement is true.");
		System.out.println("The foregoing statement is false.");
	}
}

