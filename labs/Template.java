//****************************************
// Honor Code: This work is mine unless otherwise cited.
// Kyle Donnelly
// COMPSC 111 Spring 2016
// Lab #
// Date: mmm dd yyyy
//
// Purpose: ...
//****************************************
import java.util.Date; // needed for printing today's date

public class Xxxxxx
{
	//--------------------------------
	// main method: program execution begins here
	//--------------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Kyle Donnelly\n Lab #\n" + new Date() + "\n");
	}
}

