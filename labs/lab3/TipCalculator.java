
//****************************************
// Honor Code: This work is mine unless otherwise cited.
// Kyle Donnelly
// COMPSC 111 Spring 2016
// Lab 3
// Date: 02/04/2016
//
// Purpose: To create a program that calculates the price of a theoretical restaurant bill.
//****************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner; //Allows use of Scanner class.

public class TipCalculator
{
	//--------------------------------
	// main method: program execution begins here
	//--------------------------------
	public static void main(String[] args)
	{

		Scanner scan = new Scanner(System.in);
		String name;  // Decalres the variable 'name' as a string.
		double bill;  // These statements declare variables for the bill, tip percentage, tip, total bill etc.
		double percent;
		double tip;
		double total;
		int ppl;
		double total2;
		

		// Label output with name and date:
		System.out.println("Kyle Donnelly\n Lab 3\n" + new Date() + "\n");

		System.out.println("Please enter your name.");
		name = scan.nextLine(); // This takes the user's input as a string.
		System.out.println("Welcome, " + name +"!");
		System.out.println("Please enter the bill amount.");
		bill = scan.nextDouble(); // This takes the user's input (bill) as a real number.
		System.out.println("Please enter the desired percentage you wish to tip.");
		percent = scan.nextDouble(); // This takes the user's input (percentage tipping) as a real number.
		tip = (percent/100.0)*bill;
		total = tip + bill; // This line calculates the total bill including the tip.
		System.out.println("The original bill is $" + bill + ".");
		System.out.println("The amount you wish to tip is $" + tip + ".");
		System.out.println("The total bill including the tip is $" + total +".");
		System.out.println("How many will split the bill?");
		ppl = scan.nextInt(); // This line records as a variable the number of people splitting the bill.
		total2 = total/ppl;  // This line computes how much each person in the group will pay.
		System.out.println("Each person should pay $" + total2 +".");
		System.out.println("Have a great day! Thank you for your patronage.");
		
		
		
		
			


	}
}

