import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class TodoList {

    private ArrayList<TodoItem> todoItems; // Creates a class.
    private static final String TODOFILE = "todo.txt"; // Creates string from the text file todo.txt.

    public TodoList() {
        todoItems = new ArrayList<TodoItem>();  // Creates todoItems of the class ArrayList.
    }

    public void addTodoItem(TodoItem todoItem) {
        todoItems.add(todoItem); // Adds todoItem to the list todoItems.
    }

    public Iterator getTodoItems() {
        return todoItems.iterator(); // Returns all items on the todo list to the user.
    }

    public void readTodoItemsFromFile() throws IOException { // Notifies user of error.
        Scanner fileScanner = new Scanner(new File(TODOFILE)); // Makes instance of the scanner class.
        while(fileScanner.hasNext()) {  // Loops while the fielScanner has another iteration.
            String todoItemLine = fileScanner.nextLine(); // Sets the string value to be the next line of the text file.
            Scanner todoScanner = new Scanner(todoItemLine); // Creates new scanner called todoScanner.
            todoScanner.useDelimiter(","); // Uses a comma as a delimiter for the todo scanner.
            String priority, category, task; // Makes the strings priority, category and task.
            priority = todoScanner.next(); // Allows user to set value of strings priority, category and task.
            category = todoScanner.next();
            task = todoScanner.next();
            TodoItem todoItem = new TodoItem(priority, category, task); // Makes new todoItem.
            todoItems.add(todoItem); // Adds another todo item to the list.
        }
    }

    public void markTaskAsCompleted(int toMarkId) { // Label a task as completed.
        Iterator iterator = todoItems.iterator(); // Makes an iterator called iterator.
        while(iterator.hasNext()) { // Creates loop that runs as long as there's another iteration on the list.
            TodoItem todoItem = (TodoItem)iterator.next(); // Makes a TodoItem titles todoitem that is the next line of the list TodoItem.
            if(todoItem.getId() == toMarkId) { // Gets ID number and checks if it's completed.
                todoItem.markCompleted(); // Marks task as completed if ID number equals toMarkId.
            }
        }
    }

    public Iterator findTasksOfPriority(String requestedPriority) { // Finds the tasks of a given priority.
        ArrayList<TodoItem> priorityList = new ArrayList<TodoItem>(); // Creates new list called priorityList of priority items.
        // TODO: Add source code that will find and return all tasks of the requestedPriority
        Iterator iterator = todoItems.iterator(); // Makes an iterator entitled iterator.
        while(iterator.hasNext()) { // Creates loop that runs as long as there's another iteration on the list.
            TodoItem todoItem = (TodoItem)iterator.next(); // Makes a TodoItem titles todoitem that is the next line of the list TodoItem.
            if(todoItem.getPriority().equals(requestedPriority)) { //Checks if a task is of a certain priority.
                priorityList.add(todoItem); // Adds task to the list.
            }
        }
        return priorityList.iterator();
    }

    public Iterator findTasksOfCategory(String requestedCategory) { // Finds the category of each task.
        ArrayList<TodoItem> categoryList = new ArrayList<TodoItem>(); // Creates new list called categoryList of categorized items.
        // TODO: Add source code that will find and return all tasks for the requestedCategory
        Iterator iterator = todoItems.iterator(); // Makes an iterator entitled iteration.
        while(iterator.hasNext()) { // Creates loop that runs as long as there's another iteration on the list.
            TodoItem todoItem = (TodoItem)iterator.next(); // Makes a TodoItem titles todoitem that is the next line of the list TodoItem.
            if(todoItem.getCategory().equals(requestedCategory)) { //Checks if a task is of a certain category.
                categoryList.add(todoItem); // Adds task to the list.
            }
        }

        return categoryList.iterator();
    }

    public String toString() { // Lists the ID, priority, category, task, and whether it is completed.
        StringBuffer buffer = new StringBuffer(); // Creates a StringBuffer entitled buffer.
        Iterator iterator = todoItems.iterator(); // Creates a new iterator called todoItems.iterator.
        while(iterator.hasNext()) { // Creates a loop that continues while there is still another iteration.
            buffer.append(iterator.next().toString()); // Adds on ID, priority, category, task and whether it is completed to buffer.
            if(iterator.hasNext()) {
                buffer.append("\n"); // Adds new line if there is another iteration.
            }
        }
        return buffer.toString(); // Prints the todo items and their statuses.
    }

}
