import java.io.IOException;
import java.util.*;

public class TodoListMain {

    public static void main(String[] args) throws IOException {
        System.out.println("Welcome to the Todo List Manager!");
        System.out.println("What operation would you like to perform?");
        System.out.println("Available options: read, priority-search, category-search, completed, list, quit");

        Scanner scanner = new Scanner(System.in); // Creates new scanner entitled 'scanner.'
        TodoList todoList = new TodoList(); // Creates new TodoList called todoList.

        while(scanner.hasNext()) { // Creates a loop that continues while there is another input.
            String command = scanner.nextLine(); // Creates a new string that equals the user input.
            if(command.equals("read")) {
                todoList.readTodoItemsFromFile(); // If the user enters read, the program reads items off the todoList.
            }
            else if(command.equals("list")) {
                System.out.println(todoList.toString()); // If user enters list, the program lists off properties of the task.
            }
            else if(command.equals("completed")) {
                System.out.println("What is the id of the task?");
                int chosenId = scanner.nextInt();
                todoList.markTaskAsCompleted(chosenId);// If user enter completed, the chosen task is marked as completed.
            }
            else if(command.equals("category-search")) {
                System.out.println("What is the category of the task?");
                String requestedCategory = scanner.next();
                Iterator cat = todoList.findTasksOfCategory(requestedCategory); // If user enters category-search, the program lists off properties of tasks of this category.
                while(cat.hasNext()) {
                TodoItem ti = (TodoItem)cat.next();
                System.out.println(ti);
                }
            }

            else if(command.equals("priority-search")) {
                System.out.println("What is the priority of the task?");
                String requestedPriority = scanner.next();
                Iterator pri = todoList.findTasksOfPriority(requestedPriority); // If user enters priority-search, the program lists off properties of tasks of this priority.
                while(pri.hasNext()) {
                TodoItem it = (TodoItem)pri.next();
                System.out.println(it);
                }
            }



            else if(command.equals("quit")) { // If user enters quit, the program break.
                break;
            }
        }

    }

}
