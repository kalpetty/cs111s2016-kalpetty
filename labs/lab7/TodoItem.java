public class TodoItem {

    private int id;
    private static int nextId = 0;
    private String priority;
    private String category;
    private String task;
    private boolean done;

    public TodoItem(String p, String c, String t) { // Makes class called TodoItem with three properties.
        id = nextId;
        nextId++;
        priority = p;
        category = c;
        task = t;
        done = false;
    }

    public int getId() {
        return id; // Returns ID of task.
    }

    public String getPriority() {
        return priority; // Returns priority of task.
    }

    public String getCategory() {
        return category; // Returns category of task.
    }

    public String getTask() {
        return task; // Returns name of task.
    }

    public void markCompleted() {
        done = true; // Sets done to true if the task is marked completed.
    }

    public boolean isCompleted() {
        return done; // Returns whether or not task is done.
    }

    public String toString() {
        return new String(id + ", " + priority + ", " + category + ", " + task + ", completed? " + done); // Returns properties of task.
    }

}
