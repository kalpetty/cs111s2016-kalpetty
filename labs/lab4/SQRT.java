
//****************************************
// Honor Code: This work is mine unless otherwise cited.
// Kyle Donnelly
// COMPSC 111 Spring 2016
// Lab 4
// Date: 02 11 2016
//
// Purpose: To make a program that approximates the square root of a positive integer.
//****************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;

public class SQRT
{
	//--------------------------------
	// main method: program execution begins here
	//--------------------------------
	public static void main(String[] args)
	{
        Scanner scan =new Scanner(System.in);
        double x, guess, x2;
		System.out.println("Kyle Donnelly\n Lab 4\n" + new Date() + "\n");
        System.out.println("Enter the number you want the square root of:");
        x = scan.nextDouble();
         System.out.println("Enter your best guess for the square root:");
        guess = scan.nextDouble();
        guess = .5*(guess+x/guess);
        guess = .5*(guess+x/guess);
        guess = .5*(guess+x/guess);
        guess = .5*(guess+x/guess);
        x2 = guess*guess;
        System.out.println("Original value: "+ x+ " SQRT Approximation: "+ guess + " Approx squared: "+ x2);
	}
}

