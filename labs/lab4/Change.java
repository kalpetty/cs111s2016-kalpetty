
//****************************************
// Honor Code: This work is mine unless otherwise cited.
// Kyle Donnelly
// COMPSC 111 Spring 2016
// Lab 4
// Date: 02 11 2016
//
// Purpose: To create a program that calculates the change for a given amount of money in cents.
//****************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;

public class Change
{
	//--------------------------------
	// main method: program execution begins here
	//--------------------------------
    //


    public static void main(String[] args)
	{
        Scanner scan =new Scanner(System.in);
        int bill, ten, five, one, quart, dime, nickel, penny;
		// Label output with name and date:
		System.out.println("Kyle Donnelly\n Lab 4\n" + new Date() + "\n");
        System.out.println("Enter the price of the bill in cents.");
        bill = scan.nextInt();
        ten = bill/1000;
        five = (bill%1000)/500;
        one = ((bill%1000)-500*five)/100;
        quart = (bill%100)/25;
        dime = ((bill%100)-25*quart)/10;
        nickel = ((bill%100)-25*quart-10*dime)/5;
        penny = (bill%100)-25*quart-10*dime-5*nickel;
        System.out.println("To make change for "+ bill +" cents, you need:");
        System.out.println(ten + " tens, "+ five + " fives, "+ one + " ones, "+ quart + " quarters, "+ dime + " dimes, " + nickel + " nickels, and "+ penny + " pennies.");

	}
}

