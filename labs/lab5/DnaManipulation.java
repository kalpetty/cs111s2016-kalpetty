
//****************************************
// Honor Code: This work is mine unless otherwise cited.
// Kyle Donnelly and Rayna Pelisari
// COMPSC 111 Spring 2016
// Lab 5
// Date: 02 18 2016
//
// Purpose: ...
//****************************************
import java.util.Random;
import java.util.Date; // needed for printing today's date
import java.util.Scanner;
public class DnaManipulation
{
	//--------------------------------
	// main method: program execution begins here
	//--------------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
	    System.out.println("Kyle Donnelly and Rayna Pelisari\n Lab 5\n" + new Date() + "\n");

        Scanner scan = new Scanner(System.in);
        Random rand = new Random();
        String DNA, cDNA;

        System.out.println("Please enter a DNA sequence consisting of only As,Ts,Cs, and Gs,");
        DNA = scan.nextLine(); // User inputted DNA string.
        DNA = DNA.toUpperCase(); // This capitalizes the DNA string input.
        cDNA = DNA;

        cDNA = cDNA.replace( 'A', 'X'); // This part makes a new string that is the complement of the old DNA string.
        cDNA = cDNA.replace( 'C', 'Y');
        cDNA = cDNA.replace( 'T', 'Z');
        cDNA = cDNA.replace( 'G', 'W');
        cDNA = cDNA.replace( 'X', 'T');
        cDNA = cDNA.replace( 'Y', 'G');
        cDNA = cDNA.replace( 'Z', 'A');
        cDNA = cDNA.replace( 'W', 'C');

        System.out.println("The Complement to the DNA sequence is: " + cDNA + ".");

        int length = DNA.length(); // Takes length of DNA string.
        int position = rand.nextInt(length); // Picks random position in string.
        char c = "ACGT".charAt(rand.nextInt(4)); // Picks random character in string.
        String newDNA = DNA.substring(0,position) + c + DNA.substring(position);
        System.out.println("The mutated sequence is " + newDNA + ", obtained by inserting the random character " + c + " in position " + position + " of the original.");

        position = rand.nextInt(length); // This resets the random position.
        newDNA = DNA.substring(0,position) + DNA.substring(position+1);
        char x = DNA.charAt(position); // THis resets the random character.
        System.out.println("The mutated sequence is " + newDNA + ", obtained by removing character " + x + " in position " + position + " of the original.");

        position = rand.nextInt(length);
        c = "ACGT".charAt(rand.nextInt(4));
        x = DNA.charAt(position);

        newDNA = DNA.substring(0,position)+ DNA.substring(position+1);
        newDNA = newDNA.substring(0,position)+ c + newDNA.substring(position);
        System.out.println("The mutated sequence is " + newDNA + " obtained by changing the character " + x +" to the character " + c + " at position " + position + ".");
	}


}

