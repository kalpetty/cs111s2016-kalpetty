//*************************************
// Honor Code: This work is mine unless otherwise cited.
// Kyle Donnelly
// CMPSC 111 Spring 2016
// Class Example Program
// Date: February 8, 2015
//
// Purpose: Demonstrates addition of numbers
//*************************************

import java.util.Scanner;

public class Addition
{
	public static void main( String args[] )
	{

		int first = 0;
		int second = 0;
		int sum = 0;

		Scanner input = new Scanner( System.in );

		System.out.print( "Enter first integer: " );
		first = input.nextInt();

		System.out.print( "Enter second integer: " );
		second = input.nextInt();

		sum = first + second;
		System.out.println( "Sum is " +sum );

		double sum1 = (double)first + second;
		System.out.println("Sum1 is " +sum1 );

	}
}
