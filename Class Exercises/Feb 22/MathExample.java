//=================================================
// Class Example using Math Class
// February 22, 2016
// CMPSC 111, Spring 2016
// Kyle Donnelly
//
// Purpose: Experiment with various methods from the Math class
//=================================================

import java.util.Scanner;
public class MathExample
{
  	public static void main ( String args[] )
 	{
 		double value;
 		Scanner scan = new Scanner ( System.in );
 		System.out.print ( "Enter number: " );

 		value = scan.nextDouble();

 		System.out.println ("abs: "+Math.abs(value));
 		System.out.println ("ceil: "+Math.ceil(value));
 		System.out.println ("sqrt: "+Math.sqrt(value));
 		System.out.println ("Pi: "+Math.PI);
        System.out.println ("pow: "+Math.pow(value,7));
        System.out.println (("max: "+Math.max(value,9)));



}
}
