//*************************************
// Honor Code: This work is mine unless otherwise cited.
// Kyle Donnelly
// CMPSC 111 Spring 2016
// Class Exercise
// Date: February 3, 2016
//
// Purpose: Practice using the scanner class.
//*************************************
import java.util.Date; // needed for printing today’s date
import java.util.Scanner;
public class AgeStats
{
	//----------------------------
	// main method: program execution begins here
	//----------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Briesha Bell\n " + new Date() + "\n");

       double age = 10; //declaration
	   //age= 10; //assignment statement
	   double result = 0; //declaration and assignment in one statement

		//declare a Sanner object
		Scanner userInput = new Scanner (System.in);

		System.out.println("Enter an age ");
		age = userInput.nextInt();

		// convert age to minutes
		result= age*365*24*60;
		System.out.println("Age in minutes is " +result);

       //convert age to centuries
       double ageincents=age/100;
       System.out.println("Age in centuries is " +ageincents);
	}
}
