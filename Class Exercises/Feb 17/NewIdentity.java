//Kyle Donnelly
//Computer Science 111
//February 17, 2016
//Honor Code:
//
// Purpose: to simulate the creation of
// a random new identity
// ==========================================
import java.util.Scanner;
import java.util.Random;

public class NewIdentity
{

	public static void main(String args[])
   	{
		String firstName, lastName, job;
      	int age;
      	float salary;

		// create an instance of the Scanner
        Scanner scan = new Scanner (System.in);

      	// create an instance of the Random class
      	Random rand = new Random();
		// get the user's input
		System.out.print("Please enter a first name: ");
		firstName = scan.nextLine();
		System.out.print("Please enter a last name: ");
		lastName = scan.nextLine();
		System.out.print("Please enter your dream job: ");
        job = scan.nextLine();
		System.out.print("Please enter an integer: ");
		age = scan.nextInt();
		System.out.print("Please enter a floating point number: ");
		salary = scan.nextFloat();


		// To DO: randomly change the user's input

        // 1. get the length of the firstName string
        int length = firstName.length();

        // 2. get the random position of the character in the firstName string
        int position = rand.nextInt(length);
        // 3. pick a character at that position (random letter)
        char randomLetter = firstName.charAt(position);
        System.out.println("Random letter at position "+ position+" is "+ rand);
		// 4. replace all of the selected characters with character 'a'
        firstName=firstName.replace(randomLetter,'a');
		// 5. append "ov" to the lastName string
        lastName+= "ov";
		// 6. change the job to upper case
        job = job.toUpperCase();
		// 7. assign a random value for age

		// 8. assign a random value for salary

		System.out.println("Your new name is "+firstName+" " +lastName+", and you are "+age+" years old. \nYou work as a "+job+ " making $"+salary+ " a year.");

   }
}

