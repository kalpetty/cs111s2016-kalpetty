
//*************************************
// Kyle Donnelly
// Honor Code: This work is mine unless otherwise cited.
// Computer Science 111 Spring 2016
// Date: Feb 10 2016
// Purpose: To make a program that converts temperature from Farenheit to Celcius
//*************************************
import java.util.Date;
import java.util.Scanner;
public class TempConversion
{
	//----------------------------
	// main method: program execution begins here
	//----------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
        System.out.println("Briesha Bell\n " + new Date() + "\n");
		Scanner userInput = new Scanner(System.in);
		float tempFarenheit;
		float tempCelcius;

		System.out.println("Please enter temperature in Farenheits");
		tempFarenheit = userInput.nextFloat(); // read in input and save it as a float data type

		tempCelcius = (tempFarenheit-32)*(float)5/9; // casting 5 to become a float data type
		System.out.println("The temperature in Celcius is : " + tempCelcius);

		// TO DO: ask the user to enter temperature change in F
		// Modify the result based on that change
		System.out.println("Please enter a temperature change");
		float tempChange = userInput.nextFloat();
		tempFarenheit+=tempChange;
		System.out.println("Temp F " + tempFarenheit);
		tempCelcius=(tempFarenheit-32)*(float)5/9;
		System.out.println("The temperature in Celsius is : "+tempCelcius);
        }
     }


