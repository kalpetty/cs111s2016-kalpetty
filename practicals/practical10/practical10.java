
//****************************************
// Honor Code: This work is mine unless otherwise cited.
// Kyle Donnelly
// COMPSC 111 Spring 2016
// Practical 10
// Date: 04 25 2016
//
// Purpose: To make a program that shows the events that happen in a given year.
//****************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class practical10 {
public static void main(String[] args){

    int num;
    Scanner scan = new Scanner(System.in);
    System.out.println("Enter the length in centimeters.");
    num = scan.nextInt();
    num = num/100;
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setLayout(new FlowLayout());
    frame.add(new JButton("Convert!"));
    frame.add(new JTextField(num));
    frame.add(new JLabel(num + "meters"));

    frame.setVisible(true);


}
}

