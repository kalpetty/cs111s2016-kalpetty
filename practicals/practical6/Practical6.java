//====================================
// CMPSC 111
// Practical 6
// 4 March 2016
//
// This program describes an octopus in the kitchen.
//====================================

import java.util.Date;

public class Practical6
{
    public static void main(String[] args)
    {
        System.out.println("Kyle Donnelly\n" + new Date() + "\n");

        // Variable dictionary:
        Octopus ocky, octo;           // an octopus
        Utensil spat, spork;           // a kitchen utensil

        spat = new Utensil("spatula"); // create a spatula
        spat.setColor("green");        // set spatula properties--color...
        spat.setCost(10.59);           // ... and price

        ocky = new Octopus("Ocky",15);    // create and name the octopus
        ocky.setWeight(100);           // ... weight,...
        ocky.setUtensil(spat);         // ... and favorite utensil.


        spork = new Utensil("Spork");
        spork.setColor("maroon");        // set spork properties--color...
        spork.setCost(50.00);           // ... and price




        octo = new Octopus("Octo",21);
        octo.setWeight(500);
        octo.setUtensil(spork);

        System.out.println("Testing 'get' methods:");
        System.out.println(ocky.getName() + " weighs " +ocky.getWeight()
            + " pounds\n" + "and is " + ocky.getAge()
            + " years old. His favorite utensil is a "
            + ocky.getUtensil());

        System.out.println(ocky.getName() + "'s " + ocky.getUtensil() + " costs $"
            + ocky.getUtensil().getCost());
        System.out.println("Utensil's color: " + spat.getColor());

        // Use methods to change some values:

        ocky.setAge(20);
        ocky.setWeight(125);
        spat.setCost(15.99);
        spat.setColor("blue");

        System.out.println("\nTesting 'set' methods:");
        System.out.println(ocky.getName() + "'s new age: " + ocky.getAge());
        System.out.println(ocky.getName() + "'s new weight: " + ocky.getWeight());
        System.out.println("Utensil's new cost: $" + spat.getCost());
        System.out.println("Utensil's new color: " + spat.getColor());

         System.out.println("Testing 'get' methods:");
         System.out.println(octo.getName() + " weighs " +octo.getWeight()
                 + " pounds\n" + "and is " + octo.getAge()
            + " years old. His favorite utensil is a "
            + octo.getUtensil());

         System.out.println(octo.getName() + "'s " + octo.getUtensil() + " costs $"
                 + octo.getUtensil().getCost());
         System.out.println("Utensil's color: " + spork.getColor());

        // Use methods to change some values:

        octo.setAge(25);
        octo.setWeight(430);
        spork.setCost(59.65);
        spork.setColor("black");

        System.out.println("\nTesting 'set' methods:");
        System.out.println(octo.getName() + "'s new age: " + octo.getAge());
        System.out.println(octo.getName() + "'s new weight: " + octo.getWeight());
        System.out.println("Utensil's new cost: $" + spork.getCost());
        System.out.println("Utensil's new color: " + spork.getColor());

    }
}
