
//****************************************
// Honor Code: This work is mine unless otherwise cited.
// Kyle Donnelly
// COMPSC 111 Spring 2016
// Practical #
// Date: 02 26 2016
//
// Purpose: To make a program that shows the events that happen in a given year.
//****************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;

public class YearCheck
{
	//--------------------------------
	// main method: program execution begins here
	//--------------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Kyle Donnelly\n Practical 5\n" + new Date() + "\n");
    boolean leap, bug, sun;
    int year;
    Scanner scan = new Scanner(System.in);
    System.out.println("Please enter a year between 1000 and 3000.");
    year = scan.nextInt();

    if (((year % 4 == 0 )&& (!(year % 100 == 0))) || (year % 400 == 0))
        leap = true;
    else
        leap = false;

    if ((2013-year)%17 == 0)
        bug = true;
    else
        bug = false;

    if ((2013-year)%11 == 0)
        sun = true;
    else
        sun = false;


    if (leap == true)
        System.out.print("The year " + year +" is a leap year.");
    else
         System.out.print("The year " + year +" is not a leap year.");


    if (bug == true)
        System.out.print("\nThe year " + year +" is an emergence year for Brood II of the 17-year cicadas.");
    else
        System.out.print("\nThe year " + year +" is not an emergence year for Brood II of the 17-year cicadas.");

    if (sun == true)
        System.out.print("\nThe year " + year +" is a peak sunspot year.");
    else
    System.out.print("\nThe year " + year +" is not a peak sunspot year.");

    System.out.print("\nThank you for using this program.\n");


	}
}

