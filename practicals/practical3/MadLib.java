
//****************************************
// Honor Code: This work is mine unless otherwise cited.
// Kyle Donnelly
// COMPSC 111 Spring 2016
// Practical 3
// Date: 02/05/2016
//
// Purpose: To make a Mad Libs program that stores the user's input as multiple different data types.
//****************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner; // needed to initialize scanner class

public class MadLib
{
	//--------------------------------
	// main method: program execution begins here
	//--------------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		Scanner scan = new Scanner(System.in);
		String n1;
		String a1;
		String n2;
		int i1;
		int i2;
		String v1;
		String n3;
		double d1;

		System.out.println("Kyle Donnelly\n Practical 3\n" + new Date() + "\n");

		System.out.println("Enter a plural (or group) noun.");
		n1 = scan.nextLine();
		
		System.out.println("Enter an adjective.");
		a1 = scan.nextLine();

		System.out.println("Enter another plural/group noun.");
		n2 = scan.nextLine();

		System.out.println("Enter a whole number greater than one.");
		i1 = scan.nextInt();
		scan.nextLine();

		System.out.println("Enter a plural transitive verb.");
		v1 = scan.nextLine();

		System.out.println("Enter a plural/group noun.");
		n3 = scan.nextLine();
	
		System.out.println("Enter any positive number."+"\n");
		d1 = scan.nextDouble();

		i2 = i1*i1;
	        System.out.println("----------------------------"+"\n");

		System.out.println("Dragon Facts"+"\n");
		System.out.println("Did you know all dragons love "+n1+"?");
		System.out.println("Well, except for "+a1+" dragons; they only love "+n2+".");
		System.out.println("A group of "+i1+" dragons will "+v1+" "+i2+" "+n3+".");
		System.out.println("Dragons make me skip "+d1+" feet in the air, but I like "+n3+" better.");
	}
}

