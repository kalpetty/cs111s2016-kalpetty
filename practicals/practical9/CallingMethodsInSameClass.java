//*********************************************************************************
// CMPSC 111 Spring 2016
// Practical 9
//
// Purpose: Program demonstrating  illustrates how to call static methods a class
// from a method in the same class. Static methods are the methods that can be called
// without creating an instance of the object.
//*********************************************************************************

public class CallingMethodsInSameClass
{
    public static void main(String args)
    {
        printOne();
        printThree();
        printTwo();
    }

    public static void printOne()
    {
        System.out.println("Nothing is impossible, the word itself says \"I'm possible\" !");
    }

    public static void printTwo()
    {
        printOne();
        printOne();
    }


    public static void printThree()
    {
    System.out.println("I love computer science!");

    }
}
