
//****************************************
// Honor Code: This work is mine unless otherwise cited.
// Kyle Donnelly
// COMPSC 111 Spring 2016
// Practical 2
// Date: 01 29 2016
//
// Purpose: To draw a castle out of characters.
//****************************************
import java.util.Date; // needed for printing today's date

public class MakeCastle
{
	//--------------------------------
	// main method: program execution begins here
	//--------------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Kyle Donnelly\n Practical 2\n" + new Date() + "\n");
                System.out.println("	  ___  ___    ___   ___         ");
                System.out.println("          \\  |_|  |__|  |__|  /    ");
                System.out.println("           \\_________________/ 	");
                System.out.println("   	   |		     |      ");
                System.out.println("	   |  		     |      ");
                System.out.println("	   |		     |      ");
                System.out.println("	   |		     |      ");
                System.out.println("	   |		     |      ");
                System.out.println("           |_________________|      ");

	}
}

