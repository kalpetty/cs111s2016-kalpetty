
//****************************************
// Honor Code: This work is mine unless otherwise cited.
// Kyle Donnelly and Briesha Bell
// COMPSC 111 Spring 2016
// Optional Assignment: Package
// Date: 05/08/2016
//
// Purpose: To make a program calculating shipping price in various scenarios.
//****************************************
import java.util.Scanner; // needed to initialize scanner class
import java.text.DecimalFormat; // Used for formatting.

public class Package
{
	//--------------------------------
	// main method: program execution begins here
	//--------------------------------
	public static void main(String[] args)
	{
    Scanner scan = new Scanner(System.in);
    int dist, weight, count;
    double rate, price;

    System.out.println("Welcome to the You Package it We Savage It Shipping Company.");

    boolean gate = false;
    do {
    System.out.println("Enter the shipping distance in miles:");
    dist = scan.nextInt();
    if (dist >= 1){
        gate = true;
    }

    } while(gate == false);

    gate = false;
    do {
        System.out.println("Enter the weight in pounds (1-65):");
       weight = scan.nextInt();
       if ((weight >= 1) && (weight <= 65)){
        gate = true;
    }

    } while(gate == false);

    if(weight <= 4){
         rate = 2.34;
    }
    else if (weight >= 5 && weight <= 12){
        rate = 4.31;
    }
    else if (weight >= 13 && weight <= 22){
        rate = 6.01;
    }
    else if (weight >= 23 && weight <= 45){
        rate = 6.27;
    }
    else {
        rate = 7.01;
    }

    count = dist/25;
    if(dist%25 != 0){
        count++;
    }

    price = count*rate;
    DecimalFormat df = new DecimalFormat("###0.00");
    System.out.println("Your total comes to: " + df.format(price));





    }
}

