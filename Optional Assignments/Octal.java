
//****************************************
// Honor Code: This work is mine unless otherwise cited.
// Kyle Donnelly and Briesha Bell
// COMPSCI 111 Spring 2016
// Optional Assignment: Octal
// Date: 05/08/2016
//
// Purpose: To create a program that converts an integer to octal.
//****************************************
import java.util.Scanner; // needed to initialize scanner class

public class Octal
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int n;

        System.out.println("Please enter an integer between 0 and 32767.");
        n = scan.nextInt();
        if (n > 32767){
            System.out.println("UNABLE TO CONVERT");
        }
        else {
             System.out.print("Your integer number " + n + ", in octal, is ");
             System.out.print(n/4096);
             n = n%4096;
             System.out.print(n/512);
             n = n%512;
             System.out.print(n/64);
             n = n%64;
             System.out.print(n/8);
             n = n%8;
             System.out.println(n);
        }
    }
}

